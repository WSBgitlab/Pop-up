const button = document.getElementById('button');
const body = document.body;


button.addEventListener('click', () => {    
    let popup = document.createElement('div');
    popup.setAttribute('class','popup');

    let divPop = document.createElement('section');
    divPop.setAttribute('class','sec-pop');
    
    let h1 = document.createElement('h1');
    h1.setAttribute('class','h1-popup');
    h1.appendChild(document.createTextNode('Deseja Fechar a Imagem!'));

    let formEl = document.createElement('form');
    formEl.setAttribute('action','index.php');
    formEl.setAttribute('method','post');
    formEl.setAttribute('class','form-popup');

    let btn = document.createElement('button');
    btn.setAttribute('class','btn btn-outline-dark');
    btn.appendChild(document.createTextNode('OK!'));

    body.appendChild(popup);
    popup.appendChild(divPop);
    divPop.appendChild(h1);
    divPop.appendChild(formEl);
    formEl.appendChild(btn);

});





